# Filipe Magalhães
# DRE: 117059493






# -------------Q1

def areaRetangulo (lado1, lado2):
    '''Esta função calcula a área do retângulo, dado seus dois lados.'''
    return lado1*lado2

##testes

##>>> areaRetangulo(3,5)
##15

##>>> areaRetangulo(5,7)
##35

##>>> areaRetangulo(15,2)
##30

##>>> areaRetangulo(500,700)
##350000








# -------------Q2

def areaCubo(c):
    '''Esta função calcula a área de superfície de um cubo, dado sua aresta'''
    return c ** 2 * 6

##testes

##>>> areaCubo(5)
##150

##>>> areaCubo(15)
##1350

##>>> areaCubo(150)
##135000

##>>> areaCubo(120)
##86400

##>>> areaCubo(20)
##2400






 
#-----------Q3

def areaCoroaCircular(r1,r2):
    '''Esta função calcula a área de uma coroa circular, dado seus 2 raios'''
    areaR1=3.14*r1*r1
    areaR2=3.14*r2*r2
    if r1>r2:
        resultado=areaR1-areaR2
        return resultado
    else:
        resultado=areaR2-areaR1
        return resultado

#testes
    
##>>> areaCoroaCircular(1,2)
##9.42
    
##>>> areaCoroaCircular(15,5)
##628.0
    
##>>> areaCoroaCircular(100,0)
##31400.0





#-----------Q4
def media (n1,n2):
    '''Esta fução calcula a média entre 2 números'''
    resultado = (n1+n2)/2
    return resultado

#testes
##>>> media(-5,7)
##1.0

##>>> media(2,-2)
##0.0

##>>> media(5,5)
##5.0

##>>> media(3,4)
##3.5

##>>> media(3.0,4.0)
##3.5





#------------Q5
def funcao2oGrau (a,b,c,x):
    '''Essa função calcula o resultado de uma função do 2o grau'''
    resultado = (a*x**2)+(b*x)+c
    return resultado

#testes

##>>> funcao2oGrau(3,2,1,1)
##6

##>>> funcao2oGrau(10,5,3,16)
##2643






#-----------Q6

def mediaPonderada (v1, p1, v2, p2):
    media=(v1 * p1+ v2 * p2)/(p1 + p2)
    return media

#testes

##>>> mediaPonderada(5,1,10,3)
##8.75

#-----------Q7
def erroPg(q, n):
    '''Essa função calcula algo'''
    somaPg = 1/(1-q)
    somaPrimeiros = (q ** n - 1)/(q - 1)
    erro = somaPg - somaPrimeiros
    return erro

#-----------Q8
def calculaGorjeta(conta):
    '''Essa função calcula a gorjeta estabelecida como 15%'''
    gorjeta=conta*0.15
    return 'R$ ' + str(gorjeta)

#testes

##>>> calculaGorjeta(100)
##'R$ 15.0'

##>>> calculaGorjeta(459)
##'R$ 68.85'





#-----------Q9
def escolheGorjeta(conta,gorjeta):
    '''Essa função calcula o valor da gorjeta, escolhendo sua porcentagem,
dado o valor da conta'''
    gorjetaPorcento=gorjeta/100
    gorjeta=conta*gorjetaPorcento
    return 'R$ ' + str(gorjeta)

#testes

##>>> escolheGorjeta(100,15)
##'R$ 15.0'

##>>> escolheGorjeta(200,10)
##'R$ 20.0'

##>>> escolheGorjeta(328.99,8)
##'R$ 26.319200000000002'





#-----------Q10

def calculaSaldo():
    '''Essa função não espera argumentos, rode esta função para aprender a calcular seu saldo'''
    print('rode o seguinte comando: meuSaldoFinal(saldoInicial, porcentagemDeJuros, numeroDeMeses) onde saldoInicial, porcentagemDeJuros e numeroDeMeses são números')
def meuSaldoFinal(saldoInicial, porcentagemDeJuros, numeroDeMeses):
    juros=porcentagemDeJuros/100
    saldoFinal=saldoInicial * (1 + juros * numeroDeMeses)
    return 'R$ ' + str(saldoFinal)

#testes

##>>> meuSaldoFinal(100,15,10)
##'R$ 250.0'

##>>> meuSaldoFinal(100,15,2)
##'R$ 130.0'



#-----------Q11

def distanciaCorrenteza(larguraRio, velCorrenteza, velBarco):
    '''Essa função calcula a distancia que a correnteza arrasta um barco, dado sua largura, velocidade da correnteza, e velocidade do barco'''
    distancia = larguraRio / velBarco * velCorrenteza
    return 'A distância é de ' + str(distancia) + ' metro(s)'
#testes

##>>> distanciaCorrenteza(100, 5,2)
##'A distância é de 250.0 metro(s)'

##>>> distanciaCorrenteza(14, 2,2)
##'A distância é de 14.0 metro(s)'

##>>> distanciaCorrenteza(95, 1.5,15)
##'A distância é de 9.5 metro(s)'

